#!/bin/sh

TYPE=""
#gcs
GCS_BUCKET_NAME=""
FILE_NAME=""
#remote
SOURCE_USER_NAME=""
SOURCE_HOST=""
SOURCE_PATH=""
SHOW_HELP="no"
#local
LOCAL_FILESYSTEM_SOURCE_PATH=""

CLEANUP_SCRIPT_CONFIG_LOCATION="/home/$(whoami)/gnf-cleanup-scripts/config-store/configuration_file.txt"

#to be passed by the user
LOG_DIR=""

#sys vars
info_tag="[INFO: GNF-CLEANUP-SCRIPT]"
error_tag="[ERROR: GNF-CLEANUP-SCRIPT]"

usage_() {
	echo "The usage appears here"
}
delete_old_config_() {
	rm -f ${CLEANUP_SCRIPT_CONFIG_LOCATION}
}


#sys_vars_for_log
date_time_append_candidate=$(date +%Y-%m-%d.%H_%M_%S)

default_log_root_dir_name=cleanup_logs
default_log_dir=/home/$(whoami)/${default_log_root_dir_name}/gnf-cleanup.log.${date_time_append_candidate}


config_import_script_path=/home/$(whoami)/gnf-cleanup-scripts/config-import
config_import_script_file_name=config-import-script

core_migration_script_path=/home/$(whoami)/gnf-cleanup-scripts/core-runner
core_migration_script_file_name=core-migration-script.bash

core_runner_dir=core_runner

create_log_dir_0_(){
	LOG_DIRECTORY_TMP=${1}
	mkdir -p ${LOG_DIRECTORY_TMP}

	if [ "$?" -eq 0 ] 
	then
		echo "$info_tag Log directory created successfully"
	else
		echo "$error_tag Could not create Log directory"
	fi
}



#will opt to create a default log dir
#if log directory path has not been provided by the user
#contrary to that if the user has provided the log directory path
#that contains the name of the default log directory
#then the system should ask for an alternate log directory
#the user will have an option to get out the agent shell
#by pressing the option N
new_log_dir_prompt_to_continue() {
shouldloop=true;
while $shouldloop; do
read -p "$info_tag Assign a different name for the logging directory (y/n) . . . ?" choice
case "$choice" in 
  y|Y ) 
  shouldloop=false
  echo "You have opted to assign a different name of the logging directory"
  read -p "Enter Branch to continue . . : " input
  LOG_DIR=${input}
  echo "You have entered a different log directory name as: ${LOG_DIR}"
  echo "Attempting to create log directory at ${LOG_DIR}"
  create_log_dir_0_ ${LOG_DIR}
  ;;
  n|N ) 
  shouldloop=false
  echo "You have opted not to assign a different name for the logging directory"
  echo "Since your log directory name conflicts with the default log directory"
  echo "The system cannot proceed with log directory creation; hence"
  echo "gracefully exiting the process"
  exit 100
  ;;
  * ) 
  echo "Invalid Option; Please enter either Y or n"
  shouldloop=true
  ;;
esac
done
}


create_log_dir_() {
	if [ ! -z "$LOG_DIR" ] 
	then
		# this condition will check
		# if log directory name conflicts with the 
		# default log directory
		echo "$info_tag The LOG directory has been entered by the user"
		echo "$info_tag Checking if LOG directory is valid"
		grep_res=$(echo ${LOG_DIR} | grep -w ${default_log_root_dir_name})
		#checking if the grep operation has a result
		if [ -z "$grep_res" ]
		then
			echo "$info_tag The Log directory name does not conflict"
			echo "$info_tag with the default log directory name"
			echo "$info_tag therefore creating log directory "
			echo "$info_tag with the name $LOG_DIR"
			# if the name does not conflict with the default log directory
			# then create the log directory with the name provided by the user
			create_log_dir_0_ ${LOG_DIR}
		else
			# if the name provided by the user conflicts with the log directory
			# then the system will ask if the user will opt to assign the system
			# with a new log directory name or will not
			# if yes; the system will opt for a new directory name
			# else; the system will gracefully exit the shell 
			# with an exit code 100
			echo "$error_tag The Log directory name conflicts with the "
			echo "$error_tag default log directory name"
			echo "$error_tag"
			echo "$error_tag The system cannot proceed without correctly configuring "
			echo "$error_tag the logging directory"
			echo "$error_tag INITIATING USER SYSTEM INTERACTION . . ."
			new_log_dir_prompt_to_continue
		fi
	else
		echo "$error_tag Log directory not specified . . . "
		echo "$info_tag Falling back log directory to /home/$(whoami)/cleanup_logs/"
		LOG_DIR=${default_log_dir}
		create_log_dir_0_ ${LOG_DIR}
	fi
}


#config file import
import_using_gcs_() {
	bash ~/gnf-cleanup-scripts/config-import/config-import-script.bash \
	--type=${TYPE} \
	--gcs-bucket=${GCS_BUCKET_NAME} \
	--file-name=${FILE_NAME} \
	--cleanup-script-config-location=${CLEANUP_SCRIPT_CONFIG_LOCATION} 2>&1 |  tee ${LOG_DIR}/${config_import_script_file_name}_${date_time_append_candidate}
	exit_code_from_above_script_exec=${?}

	if [ "$exit_code_from_above_script_exec" -eq 0 ]
	then
		echo "$info_tag Configuration Importer script exited with an EXIT CODE $exit_code_from_above_script_exec"
	else 
		echo "$error_tag There was some problem executing the script which is used to import config file from "
		echo "$error_tag gcloud storage bucket $GCS_BUCKET_NAME to Unix File System where the current"
		echo "$error_tag process is running"
	fi
}

import_config_file_from_remote_location_() {
	bash ~/gnf-cleanup-scripts/config-import/config-import-script.bash \
	--type=${TYPE} \
	--source-user-name=${SOURCE_USER_NAME} \
	--source-host=${SOURCE_HOST} \
	--remote-source-filepath=${SOURCE_PATH} \
	--file-name=${FILE_NAME} \
	--cleanup-script-config-location=${CLEANUP_SCRIPT_CONFIG_LOCATION} 2>&1 |  tee ${LOG_DIR}/${config_import_script_file_name}_${date_time_append_candidate}
	exit_code_from_above_script_exec=${?}

	if [ "$exit_code_from_above_script_exec" -eq 0 ]
	then
		echo "$info_tag Configuration Importer script exited with an EXIT CODE $exit_code_from_above_script_exec"
	else 
		echo "$error_tag There was some problem executing the script which is used to import config file from "
		echo "$error_tag Remote Unix File System to a Unix File System where the current"
		echo "$error_tag process is running"
	fi
}

import_config_file_from_local_filesystem_(){
	bash ~/gnf-cleanup-scripts/config-import/config-import-script.bash \
	--type=${TYPE} \
	--local-source-filepath=${LOCAL_FILESYSTEM_SOURCE_PATH} \
	--file-name=${FILE_NAME} \
	--cleanup-script-config-location=${CLEANUP_SCRIPT_CONFIG_LOCATION} 2>&1 |  tee ${LOG_DIR}/${config_import_script_file_name}_${date_time_append_candidate}
	exit_code_from_above_script_exec=${?}

	if [ "$exit_code_from_above_script_exec" -eq 0 ]
	then
		echo "$info_tag Configuration Importer script exited with an EXIT CODE $exit_code_from_above_script_exec"
	else 
		echo "$error_tag There was some problem executing the script which is used to import config file from "
		echo "$error_tag Local UNIX Filesystem to the same Unix File System where the current"
		echo "$error_tag process is running"
	fi

}


run_the_core_runner_(){
	if [ -f "$CLEANUP_SCRIPT_CONFIG_LOCATION" ]
	then
		#create the core runner sub directory for logging
		mkdir -p ${LOG_DIR}/${core_runner_dir}
		echo "$info_tag Attempting to read the configuration_file and execute the"
		echo "$info_tag core cleanup script"
		sh ~/gnf-cleanup-scripts/core-runner/core-cleanup-script.sh \
		-cpath=${CLEANUP_SCRIPT_CONFIG_LOCATION} \
		--log-dir=${LOG_DIR} 2>&1 | tee ${LOG_DIR}/${core_runner_dir}/${core_migration_script_file_name}.log._${date_time_append_candidate}
	else
		echo "$error_tag Config File does not exist"
		exit 1
	fi
}


preliminary_validations_() {
	if [ -z "$TYPE" ]
	then
		echo "$error_tag TYPE cannot be empty"
		echo "$error_tag upported values are \"GCS\", \"REMOTE\", \"LOCAL\" "
		usage_
		exit 1
	elif [ "$TYPE" = "GCS" ]
	then
		if [ -z "$GCS_BUCKET_NAME" ]
		then
			echo "$error_tag Google Cloud storage Bucket name cannot be empty when the TYPE=GCS"
			usage_
			exit 1
		elif [ -z "$FILE_NAME" ]
		then
			echo "$error_tag File name cannot be empty when type=GCS and bucket name = $GCS_BUCKET_NAME"
			usage_
			exit 1
		elif [ -z "$CLEANUP_SCRIPT_CONFIG_LOCATION" ]
		then
			echo "$error_tag Internal Error; Cleanup script (destination) location absent; "
			echo "$error_tag contact the developer of this script for more details"
			usage_
			exit 1
		fi
	elif [ "$TYPE" = "REMOTE" ]
	then
		if [ -z "$SOURCE_USER_NAME" ]
		then
			echo "$error_tag Remote user name cannot be empty when type=REMOTE"
			usage_
			exit 1
		elif [ -z "$SOURCE_HOST" ]
		then
			echo "$error_tag Remote host name cannot be empty when type=REMOTE"
			usage_
			exit 1
		elif [ -z "$SOURCE_PATH" ]
		then
			echo "$error_tag Remote file path cannot be empty when type=REMOTE"
			usage_
			exit 1
		elif [ -z "$FILE_NAME" ]
		then
			echo "$error_tag File name cannot be empty when type=REMOTE and HOST NAME=$SOURCE_HOST, username= $SOURCE_USER_NAME and source file path = $SOURCE_PATH "
			usage_
			exit 1
		elif [ -z "$CLEANUP_SCRIPT_CONFIG_LOCATION" ]
		then
			echo "$error_tag Internal Error; Cleanup script (destination) location absent; "
			echo "$error_tag contact the developer of this script for more details"
			usage_
			exit 1
		fi
	elif [ "$TYPE" = "LOCAL" ]
	then
		if [ -z "$LOCAL_FILESYSTEM_SOURCE_PATH" ]
		then
			echo "$error_tag local source file path cannot be empty when type=LOCAL"
			usage_
			exit 1
		elif [ -z "$CLEANUP_SCRIPT_CONFIG_LOCATION" ]
		then
			echo "$error_tag Internal Error; Cleanup script (destination) location absent; "
			echo "$error_tag contact the developer of this script for more details"
			usage_
			exit 1
		elif [ -z "$FILE_NAME" ]
		then
			echo "$error_tag File name cannot be empty when type=LOCAL and LOCAL_FILESYSTEM_SOURCE_PATH=${LOCAL_FILESYSTEM_SOURCE_PATH} "
			usage_
			exit 1
		fi
	elif [ "$SHOW_HELP" = "yes" ]
	then
		usage_
		exit 0
	else
		echo "$info_tag Performing configuration file import"
		echo "$info_tag All parameters validated"
	fi
}


for arg in "$@"
do
    case $arg in
        #PROJECT-ID
        --type=*|--source-file-storage-type=*)
        TYPE="${arg#*=}"
        shift # Remove --cache= from processing
        ;;
        --gcs-bucket=*)
        GCS_BUCKET_NAME="${arg#*=}"
        shift
		;;
		-suname=*|--source-user-name=*)
		SOURCE_USER_NAME="${arg#*=}"
		shift
		;;
		-h=*|--source-host=*)
		SOURCE_HOST="${arg#*=}"
		shift
		;;
		--remote-source-filepath=*)
		SOURCE_PATH="${arg#*=}"
		shift
		;;
		--local-source-filepath=*)
		LOCAL_FILESYSTEM_SOURCE_PATH="${arg#*=}"
		shift
		;;
		--file-name=*)
		FILE_NAME="${arg#*=}"
		shift
		;;
		--help)
		TYPE="SOME INVALID TYPE"
		CLEANUP_SCRIPT_CONFIG_LOCATION="some invalid migration script location"
		SHOW_HELP="yes"
		;;
		--log-dir=*)
		LOG_DIR="${arg#*=}"
		shift
		;;
		*)
		usage_
		exit 0
    esac
done

delete_old_config_
create_log_dir_
preliminary_validations_

if [ "$TYPE" = "GCS" ]
then
	import_using_gcs_
elif [ "$TYPE" = "REMOTE" ]
then
	import_config_from_remote_location_
elif [ "$TYPE" = "LOCAL" ]
then
	import_config_file_from_local_filesystem_
else
	echo "You might have entered an invalid type; check out the help document for possible values"
	usage_
	exit 1
fi
run_the_core_runner_