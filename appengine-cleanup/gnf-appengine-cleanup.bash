#!/bin/bash
#user parameters
SOURCE_PROJECT_ID=""
DELETE_ALL_SERVICES="no"
SPECIFIC_SERVICE_TO_BE_DELETED=""
SHOW_HELP="no"


#system_vars
info_tag="[INFO: APPENGINE_CLEANUP]"
error_tag="[ERROR: APPENGINE_CLEANUP]"
#array vars
declare -a SERVICE_ARRAY=()
declare -a DEFAULT_VERSIONS_ARR=()

usage_() {
	echo "usage appears here"
}

set_project_() {
	gcloud config set project ${SOURCE_PROJECT_ID}
	config_set_project_exit_code=$?
	if [ "$config_set_project_exit_code" -eq 0 ] 
	then
		echo "$info_tag Project successfully set as $SOURCE_PROJECT_ID"
	else
		echo "$error_tag Could not set the project id to $SOURCE_PROJECT_ID"
		echo "$error_tag Please check the gcloud logs above"
		exit ${config_set_project_exit_code}
	fi
}

list_google_appengine_services_() {
	services=$(gcloud app services list | grep -v "SERVICE" | cut -d" " -f1)
	SERVICE_ARRAY=(${services})

	echo "$info_tag Fetched service names and the details are given below"
	for service_name in "${SERVICE_ARRAY[@]}"; do
		echo "$info_tag Serivce Name : $service_name"
	done
}

delete_default_versions_() {
	echo "$info_tag Fetching details of the default service . . ."
	version_serving_traffic=$(gcloud app versions list --service=default| grep -e "1.00" | cut -d " " -f3)
	versions_inactive=$(gcloud app versions list --service=default | grep -v $version_serving_traffic | cut -d" " -f3| grep -v "VERSION")
	DEFAULT_VERSIONS_ARR=(${versions_inactive})
	echo "$info_tag Listing all the versions in the default services"
	for version_id in "${DEFAULT_VERSIONS_ARR[@]}"
	do
 		echo "$info_tag Attempting to delete version with VERSION__ID $version_id"
 		(sleep 5; echo y; sleep 2; echo n)| gcloud app versions delete ${version_id} --service=default
 		if [ "$?" -eq 0 ]
 		then
 			echo "$info_tag version with $version_id in default service"
 			echo "$info_tag has been deleted successfully"
 		else
 			echo "$error_tag Could not delete version with version id $version_id"
 			echo "$error_tag check for the gcloud execution logs above"
 		fi
	done
}

delete_a_service_() {
	service_name=${1}

	if [ "$service_name" = "default" ]
	then
		echo "$info_tag Since the service name is default which cannot be deleted from "
		echo "$info_tag Google App Engine, therefore, the process will proceed to"
		echo "$info_tag delete the versions of default service other than the "
		echo "$info_tag version where the network traffic is currently routed to"
		delete_default_versions_
	else
		echo "$info_tag Attempting to delete the service named as $service_name . . ."
		(sleep 5; echo y; sleep 2; echo n)| gcloud app services delete ${service_name}
		exit_code=$?
		if [ "$?" -eq 0 ]
		then
			echo "$info_tag Service with Name: ${service_name} was deleted successfully"
		else
			echo "$error_tag Could not delete service with Name:  ${service_name}"
 			echo "$error_tag check for the gcloud execution logs above" 
 			exit $exit_code
 		fi
 	fi
}

delete_services_() {
	echo "$info_tag Attempting to delete all services"
	echo "$info_tag and all the versions of default service"
	echo "$info_tag except the one which is serving traffic"
	version_serving_traffic=$(gcloud app versions list --service=default| grep -e "1.00" | cut -d " " -f3)
	echo "$info_tag The version of default service currently serving traffic is : ${version_serving_traffic}"
	for service_name in "${SERVICE_ARRAY[@]}"; do
		delete_a_service_ ${service_name}
	done
}

preliminary_validations_(){
	if [ "$SHOW_HELP" = "yes" ]
	then
		usage_
		exit 0
	fi

	echo "$info_tag Validating the inputs provided by the user"
	if [ -z "$SOURCE_PROJECT_ID" ]
	then
		echo "$error_tag Source Project Id cannot be empty"
		usage_
		exit 1
	elif [ "$DELETE_ALL_SERVICES" = "no" ]
	then
		echo "$info_tag You have not selected the --all flag"
		echo "$info_tag Verifying whether you have provided "
		echo "$info_tag the specific service name which you intend to delete"

		if [ -z "$SPECIFIC_SERVICE_TO_BE_DELETED" ]
		then
			echo "$error_tag When --all is not selected; the user has to provide"
			echo "$error_tag the name of the service which he/she intends to delete"
			usage_
		else
			echo "$info_tag the name of the service to be deleted by the user has been provided"
			echo "$info_tag by the user and the name of the service is $SPECIFIC_SERVICE_TO_BE_DELETED"
		fi
	else
		echo "$info_tag All validations complete; Continuing with"
		echo "$info_tag Cloud Appengine cleanup"
	fi
}

for arg in "$@"
do
    case $arg in
        #PROJECT-ID
       	 -spid=*|--source-project-id=*)
		 SOURCE_PROJECT_ID="${arg#*=}"
		 shift
		 ;;
		 --all)
		 DELETE_ALL_SERVICES="yes"
		 shift
		 ;;
		 --service-name=*)
		 SPECIFIC_SERVICE_TO_BE_DELETED="${arg#*=}"
		 shift
		 ;;
		 -h|--help)
		 SHOW_HELP="yes"
		 shift
		 ;;
		 *)
		SHOW_HELP="yes"
		exit 1
		 ;;
    esac
done

echo "$info_tag CLOUD APPENGINE CLEANUP"
echo "$info_tag GLOBALPAYMENTS INC."
echo 
preliminary_validations_
echo "$info_tag Setting project as $SOURCE_PROJECT_ID"
set_project_
echo "$info_tag Fetching Google Cloud AppEngine service details from the project"
echo "$info_tag and listing them below"
list_google_appengine_services_
echo "$info_tag Commencing Appengine Cleanup"
if [ "$DELETE_ALL_SERVICES" = "no" ]
then
	echo "$info_tag --all tag has not been selected; proceeding with deletion of "
	echo "$info_tag the appengine service whose name has been provided by the user"
	delete_a_service_ $SPECIFIC_SERVICE_TO_BE_DELETED
else
	delete_services_
fi
echo "end of process . . ."