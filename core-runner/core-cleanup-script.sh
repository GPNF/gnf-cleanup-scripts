#!/bin/sh

#todo 
#1. Induct PIPESTATUS in the logging section
#2. change the type of script from sh to bash
#3. implement failover with robust logging mechanism



#USER VARS
#FOR SKIPPING
SHOW_HELP=""
SKIP_PUBSUB_CLEANUP=""
SKIP_CLOUD_SQL_CLEANUP=""
SKIP_GOOGLE_APPENGINE_CLEANUP=""

#CLEAN UP VARS
CLEAN_UP_ENTIRE_CLOUD_RESOURCES="no"

#logging
LOG_DIR=""

#usr vars
PROJECT_ID=""
PATH_TO_CONFIG_FILE=""

#SPECIFICS
#PUBSUB
CLOUD_PUBSUB_TOPIC_TO_BE_DELETED=""
#CLOUD_PUBSUB_SUBSCRIPTION_TO_BE_DELETED=""

#SQL
CLOUD_SQL_INSTANCE_TO_BE_DELETED=""

#APPENGINE
GOOGLE_APPENGINE_SERVICE_TO_BE_DELETED=""

#system vars
info_tag="[INFO: CORE_RUNNER]"
error_tag="[ERROR: CORE_RUNNER]"
debug_tag="[DEBUG: CORE_RUNNER]"
temp_file_dir="/home/$(whoami)/cleanup_tmp"

usage_() {
	echo "The Usage appears here"
}

#sysvars for logging
date_time_append_candidate=$(date +%Y-%m-%d.%H_%M_%S)
pubsub_cleanup_log_dir=pubsub-cleanup.log.${date_time_append_candidate}
cloudsql_cleanup_log_dir=cloudsql_cleanup.log.${date_time_append_candidate}
appengine_cleanup_log_dir=appengine_migration.log.${date_time_append_candidate}


create_tmp_dir_() {
	mkdir -p ${temp_file_dir}
	exit_code_from_mkdir=${?}
	if [ "${exit_code_from_mkdir}" -eq 0 ]
	then
		echo "$info_tag The temp directory has been created successfully at ${temp_file_dir}"
	else
		echo "$error_tag Could Not create the temp directory successfully"
		echo "$error_tag Gracefully exiting the Pubsub cleanup process with EXIT CODE: ${exit_code_from_mkdir}"
		exit ${exit_code_from_mkdir}
	fi
}

delete_tmp_dir_() {
	rm -rf ${temp_file_dir}
	exit_code_from_rm=${?}

	if [ "${exit_code_from_rm}" -eq 0 ]
	then
		echo "$info_tag The temp directory was deleted successfully"
	else
		echo "$error_tag Could not delete the temporary directory"
		echo "$error_tag The system will however not exit from the process and continue processing"
	fi
}

create_log_dirs_() {
	echo "$info_tag Parent Log directory ${LOG_DIR}"
	mkdir -p ${LOG_DIR}
	cd ${LOG_DIR}
	echo "$info_tag Creating log directory  ${pubsub_cleanup_log_dir}"
	mkdir -p ${pubsub_cleanup_log_dir}
	echo "$info_tag Creating log directory  ${cloudsql_cleanup_log_dir}"
	mkdir -p ${cloudsql_cleanup_log_dir}
	echo "$info_tag Creating log directory  ${appengine_cleanup_log_dir}"
	mkdir -p ${appengine_cleanup_log_dir}

	if [ "$?" -eq 0 ]
	then
		echo "$info_tag Log directories created successfully"
	else
		echo "$error_tag Problem Creating log directories"
		exit 120
	fi
}


get_details_from_config_file_() {
	if [ -f "${PATH_TO_CONFIG_FILE}" ]
	then
		PROJECT_ID=$( cat ${PATH_TO_CONFIG_FILE} | grep -e PROJECT_ID| cut -d"=" -f2 )	
		SKIP_PUBSUB_CLEANUP=$( cat ${PATH_TO_CONFIG_FILE} | grep -e SKIP_PUBSUB_CLEANUP| cut -d"=" -f2 )	
		SKIP_CLOUD_SQL_CLEANUP=$( cat ${PATH_TO_CONFIG_FILE} | grep -e SKIP_CLOUD_SQL_CLEANUP| cut -d"=" -f2 )	
		SKIP_GOOGLE_APPENGINE_CLEANUP=$( cat ${PATH_TO_CONFIG_FILE} | grep -e SKIP_GOOGLE_APPENGINE_CLEANUP| cut -d"=" -f2 )	
		CLEAN_UP_ENTIRE_CLOUD_RESOURCES=$( cat ${PATH_TO_CONFIG_FILE} | grep -e CLEAN_UP_ENTIRE_CLOUD_RESOURCES| cut -d"=" -f2 )
		#SPECIFIC PARAMS
		CLOUD_PUBSUB_TOPIC_TO_BE_DELETED=$( cat ${PATH_TO_CONFIG_FILE} | grep -e CLOUD_PUBSUB_TOPIC_TO_BE_DELETED| cut -d"=" -f2 )
		#CLOUD_PUBSUB_SUBSCRIPTION_TO_BE_DELETED=$( cat ${PATH_TO_CONFIG_FILE} | grep -e CLOUD_PUBSUB_SUBSCRIPTION_TO_BE_DELETED| cut -d"=" -f2 )
		CLOUD_SQL_INSTANCE_TO_BE_DELETED=$( cat ${PATH_TO_CONFIG_FILE} | grep -e CLOUD_SQL_INSTANCE_TO_BE_DELETED| cut -d"=" -f2 )
		GOOGLE_APPENGINE_SERVICE_TO_BE_DELETED=$( cat ${PATH_TO_CONFIG_FILE} | grep -e GOOGLE_APPENGINE_SERVICE_TO_BE_DELETED| cut -d"=" -f2 )
	else
		echo "$error_tag Configuration File Not Found!!"
		echo "$error_tag The Configuration file path is: $PATH_TO_CONFIG_FILE"
		exit 101
	fi
}

debug_fun_(){
	echo "$debug_tag PROJECT_ID=$PROJECT_ID"
	echo "$debug_tag SKIP_PUBSUB_CLEANUP=$SKIP_PUBSUB_CLEANUP"
	echo "$debug_tag SKIP_CLOUD_SQL_CLEANUP=$SKIP_CLOUD_SQL_CLEANUP"
	echo "$debug_tag SKIP_GOOGLE_APPENGINE_CLEANUP=$SKIP_GOOGLE_APPENGINE_CLEANUP"
	echo "$debug_tag CLEAN_UP_ENTIRE_CLOUD_RESOURCES=$CLEAN_UP_ENTIRE_CLOUD_RESOURCES"
	echo "$debug_tag CLOUD_PUBSUB_TOPIC_TO_BE_DELETED=$CLOUD_PUBSUB_TOPIC_TO_BE_DELETED"
	echo "$debug_tag CLOUD_SQL_INSTANCE_TO_BE_DELETED=$CLOUD_SQL_INSTANCE_TO_BE_DELETED"
	echo "$debug_tag GOOGLE_APPENGINE_SERVICE_TO_BE_DELETED=$GOOGLE_APPENGINE_SERVICE_TO_BE_DELETED"
}

exec_pubsub_cleanup_with_all_(){
	bash ~/gnf-cleanup-scripts/pubsub-cleanup/pubsub-cleanup-script.bash \
	--source-project-id=${PROJECT_ID} \
	--all 2>&1 | tee ${pubsub_cleanup_log_dir}/pubsub_cleanup_run_${date_time_append_candidate}
	exit_code=${?}
	if [ "$exit_code" -eq 0 ]
	then
		echo "$info_tag All the Cloud Pub/Sub Topics and Subscriptions have been deleted succesfully"
	else
		echo "$error_tag Could not complete the Cloud Pub/Sub cleanup operation check for the gcloud "
		echo "$error_tag execution logs above (EXIT CODE: $exit_code)"
		echo "$error_tag Exiting the script"
		exit $exit_code
	fi
}

exec_pubsub_cleanup_with_specific_params_() {
	bash ~/gnf-cleanup-scripts/pubsub-cleanup/pubsub-cleanup-script.bash \
	--source-project-id=${PROJECT_ID} \
	--specific-topic-to-be-deleted=${CLOUD_PUBSUB_TOPIC_TO_BE_DELETED} 2>&1 | tee ${pubsub_cleanup_log_dir}/pubsub_cleanup_run_${date_time_append_candidate}
	#--specific-subscription-to-be-deleted=${CLOUD_PUBSUB_SUBSCRIPTION_TO_BE_DELETED}
	exit_code=${?}
	if [ "$exit_code" -eq 0 ]
	then
		echo "$info_tag Specific Cloud Pub/Sub Topic and Subscription has been deleted succesfully"
		echo "$info_tag DELETED TOPIC: ${CLOUD_PUBSUB_TOPIC_TO_BE_DELETED}"
		#echo "$info_tag DELETED SUBSCRIPTION: ${CLOUD_PUBSUB_SUBSCRIPTION_TO_BE_DELETED}"
	else
		echo "$error_tag Could not complete the Cloud Pub/Sub cleanup operation check for the gcloud "
		echo "$error_tag execution logs above (EXIT CODE: $exit_code)"
		echo "$error_tag Exiting the script"
		exit $exit_code
	fi
}

exec_cloud_sql_cleanup_with_all_() {
	bash ~/gnf-cleanup-scripts/cloudsql-cleanup/cloud-sql-cleanup-script.bash \
	--source-project-id=${PROJECT_ID} \
	--all 2>&1 | tee ${cloudsql_cleanup_log_dir}/cloudsql_cleanup_run_${date_time_append_candidate}
	exit_code=${?}
	if [ "$exit_code" -eq 0 ]
	then
		echo "$info_tag All the CloudSQL instances in the project $PROJECT_ID"
		echo "$info_tag have been deleted from the project succesfully!"
	else
		echo "$error_tag Could not complete the CloudSQL cleanup operation check for the gcloud"
		echo "$error_tag execution logs above (EXIT CODE: $exit_code)"
		echo "$error_tag Exiting the script"
		exit $exit_code
	fi
}

exec_cloud_sql_cleanup_with_specific_params_() {
	bash ~/gnf-cleanup-scripts/cloudsql-cleanup/cloud-sql-cleanup-script.bash \
	--source-project-id=${PROJECT_ID} \
	--instance-name=${CLOUD_SQL_INSTANCE_TO_BE_DELETED} 2>&1 | tee ${cloudsql_cleanup_log_dir}/cloudsql_cleanup_run_${date_time_append_candidate}
	exit_code=${?}
	if [ "$exit_code" -eq 0 ]
	then
		echo "$info_tag Specific CloudSQL instance in the project $PROJECT_ID"
		echo "$info_tag has been deleted from the project succesfully!"
		echo "$info_tag CLOUD SQL INSTANCE NAME: ${CLOUD_SQL_INSTANCE_TO_BE_DELETED}"
	else
		echo "$error_tag Could not complete the CloudSQL cleanup operation check for the gcloud"
		echo "$error_tag execution logs above (EXIT CODE: $exit_code)"
		echo "$error_tag Exiting the script"
		exit $exit_code
	fi
}

exec_google_appengine_cleanup_with_all_() {
	bash ~/gnf-cleanup-scripts/appengine-cleanup/gnf-appengine-cleanup.bash \
	--source-project-id=${PROJECT_ID} \
	--all 2>&1 | tee ${appengine_cleanup_log_dir}/appengine_cleanup_run_${date_time_append_candidate}
	exit_code=${?}
	if [ "$exit_code" -eq 0 ]
	then
		echo "$info_tag All Google Appengine Services have been deleted"
		echo "$info_tag Except the default service since a version of"
		echo "$info_tag Default service is receiving traffic and CANNOT"
		echo "$info_tag be deleted"
	else
		echo "$error_tag Could not Complete the Google AppEngine Cleanup Operation"
		echo "$error_tag check for the gcloud execution logs above (EXIT CODE: $exit_code)"
		echo "$error_tag Exiting the script"
		exit $exit_code
	fi
}

exec_google_appengine_cleanup_with_specific_params_() {
	bash ~/gnf-cleanup-scripts/appengine-cleanup/gnf-appengine-cleanup.bash \
	--source-project-id=${PROJECT_ID} \
	--service-name=${GOOGLE_APPENGINE_SERVICE_TO_BE_DELETED} 2>&1 | tee ${appengine_cleanup_log_dir}/appengine_cleanup_run_${date_time_append_candidate}
	#TODO: induct pipestatus in this section 

	exit_code=${?}
	if [ "$exit_code" -eq 0 ]
	then
		echo "$info_tag Specific Google Appengine Service has been deleted"
		echo "$info_tag Except the default service since a version of"
		echo "$info_tag Default service is receiving traffic and CANNOT"
		echo "$info_tag be deleted"
		echo "$info_tag SERVICE NAME: ${GOOGLE_APPENGINE_SERVICE_TO_BE_DELETED}"
	else
		echo "$error_tag Could not Complete the Google AppEngine Cleanup Operation"
		echo "$error_tag check for the gcloud execution logs above (EXIT CODE: $exit_code)"
		echo "$error_tag Exiting the script"
		exit $exit_code
	fi
}

prompt_to_continue_() {
shouldloop=true;
while $shouldloop; do
read -p "ARE YOU ABSOLUTELY SURE (y/n) . . . ?" choice
case "$choice" in 
  y|Y ) 
  shouldloop=false
  echo "$info_tag Continuing with the operations"
  ;;
  n|N ) 
  shouldloop=false
  echo "Exiting Operation as you chose not to continue"
  exit 0
  ;;
  * ) 
  echo "Invalid Option; Please enter either Y or n"
  shouldloop=true
  ;;
esac
done
}

warning_dialogue_for_all_resource_deletion_() {
	echo "$info_tag All the Google Cloud Resources used by the PROJECT: $PROJECT_ID"
	echo "$info_tag have been requested to be deleted"
	echo "$info_tag Continuing further"
	echo
	echo
	echo "$info_tag WARNING!!! "
	echo "$info_tag Since CLEAN_UP_ENTIRE_CLOUD_RESOURCES has been set to \"yes\" in the Configuration file"
	echo "$info_tag therefore all the Cloud resources will be deleted"
	echo
	echo "$info_tag you will be asked if you want to continue; an option n will be a NO"
	echo "$info_tag and the script will be exited safely"
	echo
}

do_when_entire_resources_are_requested_to_be_cleaned_up_() {
 	warning_dialogue_for_all_resource_deletion_
	prompt_to_continue_
	echo 
	echo
	if [ "$SKIP_PUBSUB_CLEANUP" = 'no' ]
	then
		echo "$info_tag COMMENCING PUBSUB CLEAN UP PROCESS"
		echo "$info_tag IMPACT: Entire Cloud Pub/Sub topics and Subscriptions will be deleted"
		exec_pubsub_cleanup_with_all_
	else
		echo "$info_tag Cloud Pub/Sub clean up process has been SKIPPED by the user"
	fi

	if [ "$SKIP_CLOUD_SQL_CLEANUP" = 'no' ]
	then
		echo "$info_tag COMMENCING CLOUD SQL CLEAN UP PROCESS"
		echo "$info_tag IMPACT: All the Cloud SQL Instances will be wiped off from the Google Cloud Project"
		exec_cloud_sql_cleanup_with_all_
	else
		echo "$info_tag Cloud SQL clean up process has been SKIPPED by the user"
	fi

	if [ "$SKIP_GOOGLE_APPENGINE_CLEANUP" = 'no' ]
	then
		echo "$info_tag COMMENCING GOOGLE APP ENGINE CLEAN UP PROCESS "
		echo "$info_tag IMPACT: All the GAE Services will be deleted along with their versions"
		echo "$info_tag only the version of the default service, currently serving traffic will not"
		echo "$info_tag be deleted"
		exec_google_appengine_cleanup_with_all_
	else
		echo "$info_tag Google App Engine Cleanup process has been SKIPPED by the user"
	fi
}

do_when_only_specific_resources_are_requested_to_be_cleaned_up_() {
	echo "$info_tag Specific Google Cloud Resources used by the PROJECT: $PROJECT_ID"
	echo "$info_tag have been requested to be deleted"
	echo "$info_tag Continuing further"
	echo
	if [ "$SKIP_PUBSUB_CLEANUP" = 'no' ]
	then
		echo "$info_tag COMMENCING PUBSUB CLEAN UP PROCESS"
		exec_pubsub_cleanup_with_specific_params_
	else
		echo "$info_tag Cloud Pub/Sub clean up process has been SKIPPED by the user"
	fi

	if [ "$SKIP_CLOUD_SQL_CLEANUP" = 'no' ]
	then
		echo "$info_tag COMMENCING CLOUD SQL CLEAN UP PROCESS"
	    exec_cloud_sql_cleanup_with_specific_params_
	else
		echo "$info_tag Cloud SQL clean up process has been SKIPPED by the user"
	fi

	if [ "$SKIP_GOOGLE_APPENGINE_CLEANUP" = 'no' ]
	then
		echo "$info_tag COMMENCING GOOGLE APP ENGINE CLEAN UP PROCESS "
		echo "$info_tag Note: The version of the default service, currently serving traffic will not"
		echo "$info_tag be deleted"
		exec_google_appengine_cleanup_with_specific_params_
	else
		echo "$info_tag Google App Engine Cleanup process has been SKIPPED by the user"
	fi

}

exec_all_the_processes_() {
	#check if entire resources have requested to be deleted
	#for each sub process; check if the sub process execution has been skipped
	if [ "$CLEAN_UP_ENTIRE_CLOUD_RESOURCES" = 'yes' ]
	then
		do_when_entire_resources_are_requested_to_be_cleaned_up_
	else
		do_when_only_specific_resources_are_requested_to_be_cleaned_up_
	fi
}


preliminary_validations_() {
 if [ "$SHOW_HELP" = 'yes' ]
 then
 	usage_
 	exit 0
 fi

 if [ -z "$PATH_TO_CONFIG_FILE" ]
 then
 	echo "$error_tag Configuration file path needs to be provided for the script to run"
 	usage_
 	exit 1
 fi
}

for arg in "$@"
do
    case $arg in
       #PROJECT-ID
       -cpath=*|--path-to-config-file=*)
	   PATH_TO_CONFIG_FILE="${arg#*=}"
	   shift
	   ;;
	   -h|--help)
	   SHOW_HELP="yes"
	   shift
	   ;;
	   -log=*|--log-dir=*)
	   LOG_DIR="${arg#*=}"
	   shift
	   ;;
	   *)
	   SHOW_HELP="yes"
	   exit 1
	   ;;
    esac
done

echo "$info_tag CLOUD CLEANUP SCRIPT"
echo "$info_tag GLOBALPAYMENTS INC."
echo 
echo "$info_tag creating temp directory for cleanup"
create_tmp_dir_
echo
echo "$info_tag Creating log directories"
create_log_dirs_
echo "$info_tag Performing preliminary level validations"
preliminary_validations_
echo "$info_tag Configuration file path as provided by the user is "
echo "$info_tag ${PATH_TO_CONFIG_FILE}"
echo "$info_tag Reading Configuration file . . ."
get_details_from_config_file_
echo "$info_tag Finished reading Configuration file "
echo "$info_tag Displaying the intialized params below"
debug_fun_
echo
echo "$info_tag Commencing clean up process"
exec_all_the_processes_
echo "$info_tag End of Operation; attempting to delete the cleanup_tmp directory"
delete_tmp_dir_
echo "$info_tag END of Process"