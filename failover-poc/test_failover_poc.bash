PROJECT_ID=pid-gousenapb-noti-res02
subscription_id=subX020203430

info_tag="[INFO]"
error_tag="[ERROR]"

subscription_del_failover="subscription_del_failover"

set_project_tmp_log_file="set_project_log_temp"
set_project_log_file="set_project_log_file"

subscription_delete_tmp_log_file="subscription_delete_tmp_log_file"
subscription_delete_log_file="subscription_delete_log_file"


#delete the temp files
rm $set_project_log_file
rm $set_project_tmp_log_file
rm $subscription_delete_tmp_log_file
rm $subscription_delete_log_file


gcloud config set project ${PROJECT_ID} >> ${set_project_tmp_log_file}
EXIT_CODE_SET_PROJECT=${?}

if [ "$EXIT_CODE_SET_PROJECT" -eq 0 ]
then
	echo "$info_tag Project set successfully" >> ${set_project_tmp_log_file}
	echo "$info_tag persisting the log" 
	mv ${set_project_tmp_log_file} ${set_project_log_file}
else
	echo "$error_tag Project not set successfully"
fi

echo "$info_tag Searching for failover files for subscription delete operation"
if [ -f  "$subscription_del_failover" ]
then
	echo "$info_tag Failover file found for subscriptions delete"
	echo "$info_tag Running the failover"
	command=$(cat ${subscription_del_failover} | cut -d"=" -f2)
	echo "$info_tag Retrieved command $command, Running now"
	eval ${command}
	EXIT_CODE_FAILOVER=$?
	if [ "$EXIT_CODE_FAILOVER" -eq 0 ] 
	then
		echo "$info_tag Failover ran successfully"
	else
		echo "$error_tag Failover did not run; exiting the code"
		exit $EXIT_CODE_FAILOVER
	fi
else
	echo "$info_tag No Failovers found, running command from fresh"
	gcloud pubsub subscriptions delete ${subscription_id} >> ${subscription_delete_tmp_log_file}
	EXIT_CODE_SUBSCRIPTION_DELETE=${?}

	if [ "$EXIT_CODE_SUBSCRIPTION_DELETE" -eq 0 ]
	then
		echo "$info_tag Subscription deleted successfully" >> ${subscription_delete_tmp_log_file}
		echo "$info_tag Persisting the log"
		mv ${subscription_delete_tmp_log_file} ${subscription_delete_log_file}
	else 
		echo "There was a problem deleting subscription"
		#start checkpointing
		echo "checkpointing data will be written to a file"
		echo "Writing failover data for next run"
		touch ${subscription_del_failover}
		echo "subscription_id=${subscription_id}" >> $subscription_del_failover 
		#write the command
		echo "FAILOVER_COMMAND=gcloud pubsub subscriptions delete $subscription_id" > $subscription_del_failover
	fi
fi