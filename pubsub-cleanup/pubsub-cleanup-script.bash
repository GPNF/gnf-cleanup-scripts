#!/bin/bash
#USER PARAMETERS
SOURCE_PROJECT_ID=""
DELETE_ALL_TOPICS_AND_SUBCRIPTIONS="no"
SPECIFIC_TOPIC_TO_BE_DELETED=""
SPECIFIC_SUBSCRIPTION_TO_BE_DELETED=""
SHOW_HELP="no"


declare -a ARRAY_TOPICS=()
declare -a ARRAY_SUBSCRIPTIONS=()
declare -a ARRAY_TOPICS_MODEFIED=()
declare -a ARRAY_SUBSCRIPTIONS_MODEFIED=()

declare -a ARRAY_TOPICS_FROM_TOPIC_FILE=()
declare -a ARRAY_TOPICS_FROM_TOPIC_FILE_MODEFIED=()
declare -a TOPIC_WISE_SUB_ARRAY=()
declare -a TOPIC_WISE_ARRAY_SUBSCRIPTIONS_MODEFIED=()

#system_vars
info_tag="[INFO: PUBSUB_CLEANUP]"
error_tag="[ERROR: PUBSUB_CLEANUP]"

temp_file_dir="/home/$(whoami)/cleanup_tmp"
temp_topic_file_name="tempTopicfile"
temp_subscription_file_name="tempSubscriptionFile"


temp_topic_file_path="${temp_file_dir}/${temp_topic_file_name}"
temp_subscription_file_path="${temp_file_dir}/${temp_subscription_file_name}"

usage_(){
 echo "Usage will appear here"
}


set_project_() {
	gcloud config set project ${SOURCE_PROJECT_ID}
	if [ "$?" -eq 0 ] 
	then
		echo "$info_tag Project successfully set as $SOURCE_PROJECT_ID"
		echo "$info_tag Removing temporary topic file"
	    #rm -f tempTopicfile
	    rm -f ${temp_topic_file_path}
	    echo "$info_tag Creating temporary topic file"
	    touch ${temp_topic_file_path}
	    chmod 644 ${temp_topic_file_path}
	    echo "$info_tag Removing temporary subscription file"
	    #set source project id in config
		rm -f ${temp_subscription_file_path}
		#dump the list of subscriptions onto a temp file
		touch ${temp_subscription_file_path}
		echo "$info_tag Creating temporary subscription file"
		chmod 644 ${temp_subscription_file_path}
	else
		echo "$error_tag Could not set project as $SOURCE_PROJECT_ID"
		echo "$error_tag Please check the gcloud logs above"
		echo "$error_tag or set a valid project id"
	fi
}	

fetch_pubsub_topic_details_from_source_() {
	gcloud pubsub topics list >>  ${temp_topic_file_path}
	topicNames=$(cat tempTopicfile | grep name | cut -d":" -f2)
	ARRAY_TOPICS_FROM_TOPIC_FILE=($topicNames)
}

fetch_pubsub_subscription_details_from_source_(){
	gcloud pubsub subscriptions list >> ${temp_subscription_file_path}
    echo "$info_tag Captured pubsub details into a file"
	topicNames=$(cat ${temp_subscription_file_path} | grep topic | cut -d":" -f2 $MYDIR| awk '{print $1}')
	subscriptionNames=$(cat ${temp_subscription_file_path} | grep name | cut -d":" -f2 $MYDIR| awk '{print $1}')
	pushEndPoints=$(cat ${temp_subscription_file_path} | grep pushEndpoint | cut -d" " -f4 $MYDIR| awk '{print $1}')
	messageRetentionDuration=$(cat ${temp_subscription_file_path} | grep messageRetentionDuration | cut -d":" -f2 $MYDIR| awk '{print $1}')
	ARRAY_TOPICS=($topicNames)
	ARRAY_SUBSCRIPTIONS=($subscriptionNames)	
}


modify_arrays_(){

for topic_names in "${ARRAY_TOPICS[@]}"
do
	blank_str=""
	#echo $topic_names
	ARRAY_TOPICS_MODEFIED+=("${topic_names//"projects/pid-gousenapb-noti-res02/topics/"/$blank_str}")
done
for topic_names in "${ARRAY_SUBSCRIPTIONS[@]}"
do
	blank_str=""
	#echo $topic_names
	ARRAY_SUBSCRIPTIONS_MODEFIED+=("${topic_names//"projects/pid-gousenapb-noti-res02/subscriptions/"/$blank_str}")   
done
for topic_names in "${ARRAY_TOPICS_FROM_TOPIC_FILE[@]}"
do
	blank_str=""
	#echo $topic_names
	ARRAY_TOPICS_FROM_TOPIC_FILE_MODEFIED+=("${topic_names//"projects/pid-gousenapb-noti-res02/topics/"/$blank_str}")   
done
}


show_array_topics_() {
	for array_topic_elem in "${ARRAY_TOPICS[@]}"; do
		#statements
		echo "$info_tag ARRAY_TOPICS: $array_topic_elem"
	done
}

show_array_subscription_elements_() {
	for array_subscription_elem in "${ARRAY_SUBSCRIPTIONS[@]}"; do
		#statements
		echo "$info_tag ARRAY_SUBSCRIPTIONS : $array_subscription_elem"
	done
}

show_array_topic_element_modefied_() {
	for array_topic_element_modefied in "${ARRAY_TOPICS_FROM_TOPIC_FILE_MODEFIED[@]}"; do
		#statements
		echo "$info_tag ARRAY_TOPICS_FROM_TOPIC_FILE_MODEFIED : $array_topic_element_modefied"
	done
}

show_array_subscriptions_modefied_(){
	for array_subscription_element_modefied in "${ARRAY_SUBSCRIPTIONS_MODEFIED[@]}"; do
		#statements
		echo "$info_tag ARRAY_SUBSCRIPTIONS_MODEFIED : $array_subscription_element_modefied"
	done
}

delete_subscriptions_modefied_(){
	subName=$1
	gcloud pubsub subscriptions delete ${subName} 
	if [ "$?" -eq 0 ]
	then
		echo "$info_tag ${subName} has been deleted"
	else
		echo "$error_tag failed to delete ${subName}"
		echo "$error_tag check for logs of the gcloud command (executed) above"
	fi
}

delete_topics_modefied_(){
	topicName=$1
	gcloud pubsub topics delete ${topicName}
	if [ "$?" -eq 0 ]
	then
		echo "$info_tag ${topicName} has been deleted"
	else
		echo "$error_tag failed to delete ${topicName}"
		echo "$error_tag check for logs of the gcloud command (executed) above"
	fi
}

delete_topics_and_subscriptions_() {
	echo "$info_tag deleting subscriptions" 
	for modefied_subscription_name in "${ARRAY_SUBSCRIPTIONS_MODEFIED[@]}"; do
		#statements
		echo "$info_tag deleting subscription: $modefied_subscription_name"
		delete_subscriptions_modefied_ $modefied_subscription_name
	done

	echo "$info_tag deleting topics"
	for modefied_topic_name in  "${ARRAY_TOPICS_FROM_TOPIC_FILE_MODEFIED[@]}"; do
		#statements
		echo "$info_tag deleting topic: $modefied_topic_name"
		delete_topics_modefied_ $modefied_topic_name
	done
}

#patch code
delete_an_entire_topic_() {
	topic_name=${1}

	subscription_data=$(gcloud pubsub topics list-subscriptions $topic_name | grep -v "^-")
	TOPIC_WISE_SUB_ARRAY=($subscription_data)

	for topic_names in "${TOPIC_WISE_SUB_ARRAY[@]}"
	do
		blank_str=""
		#echo $topic_names
		echo $topic_names
		altered="${topic_names//"projects/pid-gousenapb-noti-res02/subscriptions/"/$blank_str}"
		echo "Subscription in topic $topic_name is $altered"
		TOPIC_WISE_ARRAY_SUBSCRIPTIONS_MODEFIED+=( ${altered} )   
	done

	#delete entire topic subscriptions
	for mod_sub_name in "${TOPIC_WISE_ARRAY_SUBSCRIPTIONS_MODEFIED[@]}"
	do
		echo "$info_tag deleting subscription under topic $topic_name"
		echo "$info_tag Subscription name to be deleted : $mod_sub_name"
		#delete_subscriptions_modefied_ $mod_sub_name
		gcloud pubsub subscriptions delete $mod_sub_name
	done
	#now delete the topic
	delete_topics_modefied_ $topic_name
}

preliminary_validations_(){
	if [ "$SHOW_HELP" = "yes" ]
	then
		usage_
		exit 0
	fi

	#validations
	echo "$info_tag Validating the inputs provided by the user"
	if [ -z "$SOURCE_PROJECT_ID" ]
	then
		echo "$error_tag Source Project Id cannot be empty"
		usage_
		exit 1
	elif [ "$DELETE_ALL_TOPICS_AND_SUBCRIPTIONS" = "no" ]
	then
		echo "$info_tag You have not selected the --all flag"
		echo "$info_tag Verifying whether you have provided "
		echo "$info_tag the specific topic and subscription name"
		echo "$info_tag that you want to delete"
		if [ -z "$SPECIFIC_TOPIC_TO_BE_DELETED" ]
		then
			echo "$error_tag Specific topic to be deleted must be "
			echo "$error_tag provided by the end user when he/she"
			echo "$error_tag wishes to delete a specific topic/subscription"
			echo "$error_tag for deletion"
			usage_
			exit 1
		else 
			echo "$info_tag Topic name has been provided for deletion"
		fi
	else
		echo "$info_tag All validations complete; Continuing with"
		echo "$info_tag Cloud Pub/Sub deletion"
	fi
}

for arg in "$@"
do
    case $arg in
        #PROJECT-ID
       	 -spid=*|--source-project-id=*)
		 SOURCE_PROJECT_ID="${arg#*=}"
		 shift
		 ;;
		 --all)
		 DELETE_ALL_TOPICS_AND_SUBCRIPTIONS="yes"
		 shift
		 ;;
		 -topic=*|--specific-topic-to-be-deleted=*)
		 SPECIFIC_TOPIC_TO_BE_DELETED="${arg#*=}"
		 shift
		 ;;
		 -h|--help)
		 SHOW_HELP="yes"
		 shift
		 ;;
    esac
done

echo "$info_tag CLOUD PUBSUB CLEANUP"
echo "$info_tag GLOBALPAYMENTS INC."
echo 
preliminary_validations_

echo "$info_tag Setting project as $SOURCE_PROJECT_ID"
set_project_
echo "$info_tag Fetching pubsub (TOPICS) details from source"
fetch_pubsub_topic_details_from_source_
echo "$info_tag Fetching pubsub (SUBSCRIPTIONS) details from source"
fetch_pubsub_subscription_details_from_source_
echo "Modifying arrays to remove project related information"
modify_arrays_
echo "$info_tag echoing out topic elements (_modefied_)"
show_array_topic_element_modefied_
echo "$info_tag echoing out subscription elements (_modefied_)"
show_array_subscriptions_modefied_
if [ "$DELETE_ALL_TOPICS_AND_SUBCRIPTIONS" = "no" ]
then
	#delete_subscriptions_modefied_ ${SPECIFIC_SUBSCRIPTION_TO_BE_DELETED}
	#delete_topics_modefied_ ${SPECIFIC_TOPIC_TO_BE_DELETED}
	delete_an_entire_topic_ ${SPECIFIC_TOPIC_TO_BE_DELETED}
else
	delete_topics_and_subscriptions_
fi


