#!/bin/bash

TYPE=""
#gcs
GCS_BUCKET_NAME=""
FILE_NAME=""
#remote
SOURCE_USER_NAME=""
SOURCE_HOST=""
SOURCE_PATH=""
SHOW_HELP="no"

#local
LOCAL_FILESYSTEM_SOURCE_PATH=""
CLEANUP_SCRIPT_CONFIG_LOCATION=""

#sys vars
info_tag="[INFO: CONFIG_IMPORT]"
error_tag="[ERROR: CONFIG_IMPORT]"

import_config_from_gcs_bucket_() {
	gsutil cp gs://${GCS_BUCKET_NAME}/${FILE_NAME} ${CLEANUP_SCRIPT_CONFIG_LOCATION}

	if [ "$?" -eq 0 ]
	then
		echo "$info_tag Config file with name ${FILE_NAME} has been Successfully imported. Below are the stats"
		echo "$info_tag Source Type: Google Cloud Storage "
		echo "$info_tag Bucket Name: ${GCS_BUCKET_NAME} "
		echo "$info_tag Destination: ${CLEANUP_SCRIPT_CONFIG_LOCATION} "
		exit $?
	else 
		echo "$error_tag There was a problem importing the config file "
		echo "$error_tag Source : GCS"
		echo "$error_tag Bucket name: ${GCS_BUCKET_NAME}"
		echo "$error_tag File name: ${FILE_NAME}"
		exit $?
	fi
}


import_config_from_remote_location_(){
	scp ${SOURCE_USER_NAME}@${SOURCE_HOST}:${SOURCE_PATH}/${FILE_NAME} ${CLEANUP_SCRIPT_CONFIG_LOCATION}
	if [ "$?" -eq 0 ]
	then
		echo "$info_tag Config file with name ${FILE_NAME} has been Successfully imported. Below are the stats"
		echo "$info_tag Source Type: Remote Machine "
		echo "$info_tag HOST: ${SOURCE_HOST} "
		echo "$info_tag Username: ${SOURCE_USER_NAME} "
		exit $?
	else 
		echo "$error_tag There was a problem importing the config file "
		echo "$error_tag Source Type: Remote Machine "
		echo "$error_tag HOST: ${SOURCE_HOST} "
		echo "$error_tag Username: ${SOURCE_USER_NAME} "
		exit $?
	fi
}


import_config_file_from_local_filesystem_() {
	cp ${LOCAL_FILESYSTEM_SOURCE_PATH}/${FILE_NAME} ${CLEANUP_SCRIPT_CONFIG_LOCATION}
	if [ "$?" -eq 0 ]
	then
		echo "$info_tag Config file with name ${FILE_NAME} has been Successfully imported. Below are the stats"
		echo "$info_tag Source Type: Local Machine "
		echo "$info_tag Username: $(whoami) "
		exit $?
	else 
		echo "$error_tag There was a problem importing the config file "
		echo "$error_tag Source Type: Local Machine "
		echo "$error_tag Username: $(whoami) "
		exit $?
	fi
}

usage_() {
	echo "usage will appear here"
}

preliminary_validations_() {
	if [ -z "$TYPE" ]
	then
		echo "$error_tag TYPE cannot be empty"
		echo "$error_tag Supported values are \"GCS\", \"REMOTE\", \"LOCAL\" "
		usage_
		exit 1
	elif [ "$TYPE" = "GCS" ]
	then
		if [ -z "$GCS_BUCKET_NAME" ]
		then
			echo "$error_tag Google Cloud storage Bucket name cannot be empty when the TYPE=GCS"
			usage_
			exit 1
		elif [ -z "$FILE_NAME" ]
		then
			echo "$error_tag File name cannot be empty when type=GCS and bucket name = $GCS_BUCKET_NAME"
			usage_
			exit 1
		elif [ -z "$CLEANUP_SCRIPT_CONFIG_LOCATION" ]
		then
			echo "$error_tag Internal Error; Cleanup script (destination) location absent; "
			echo "$error_tag contact the developer of this script for more details"
			usage_
			exit 1
		fi
	elif [ "$TYPE" = "REMOTE" ]
	then
		if [ -z "$SOURCE_USER_NAME" ]
		then
			echo "$error_tag Remote user name cannot be empty when type=REMOTE"
			usage_
			exit 1
		elif [ -z "$SOURCE_HOST" ]
		then
			echo "$error_tag Remote host name cannot be empty when type=REMOTE"
			usage_
			exit 1
		elif [ -z "$SOURCE_PATH" ]
		then
			echo "$error_tag Remote file path cannot be empty when type=REMOTE"
			usage_
			exit 1
		elif [ -z "$FILE_NAME" ]
		then
			echo "$error_tag File name cannot be empty when type=REMOTE and HOST NAME=$SOURCE_HOST, username= $SOURCE_USER_NAME and source file path = $SOURCE_PATH "
			usage_
			exit 1
		elif [ -z "$CLEANUP_SCRIPT_CONFIG_LOCATION" ]
		then
			echo "$error_tag Internal Error; Cleanup script (destination) location absent; "
			echo "$error_tag contact the developer of this script for more details"
			usage_
			exit 1
		fi
	elif [ "$TYPE" = "LOCAL" ]
	then
		if [ -z "$LOCAL_FILESYSTEM_SOURCE_PATH" ]
		then
			echo "$error_tag local source file path cannot be empty when type=LOCAL"
			usage_
			exit 1
		elif [ -z "$CLEANUP_SCRIPT_CONFIG_LOCATION" ]
		then
			echo "$error_tag Internal Error; Cleanup script (destination) location absent; "
			echo "$error_tag contact the developer of this script for more details"
			usage_
			exit 1
		elif [ -z "$FILE_NAME" ]
		then
			echo "$error_tag File name cannot be empty when type=LOCAL and LOCAL_FILESYSTEM_SOURCE_PATH=${LOCAL_FILESYSTEM_SOURCE_PATH} "
			usage_
			exit 1
		fi
	elif [ "$SHOW_HELP" = "yes" ]
	then
		usage_
		exit 0
	else
		echo "$info_tag Performing configuration file import"
		echo "$info_tag All parameters validated"
	fi
}


for arg in "$@"
do
    case $arg in
        #PROJECT-ID
        --type=*|--source-file-storage-type=*)
        TYPE="${arg#*=}"
        shift # Remove --cache= from processing
        ;;
        --gcs-bucket=*)
        GCS_BUCKET_NAME="${arg#*=}"
        shift
		;;
		-suname=*|--source-user-name=*)
		SOURCE_USER_NAME="${arg#*=}"
		shift
		;;
		-h=*|--source-host=*)
		SOURCE_HOST="${arg#*=}"
		shift
		;;
		--remote-source-filepath=*)
		SOURCE_PATH="${arg#*=}"
		shift
		;;
		--local-source-filepath=*)
		LOCAL_FILESYSTEM_SOURCE_PATH="${arg#*=}"
		shift
		;;
		--cleanup-script-config-location=*)
		CLEANUP_SCRIPT_CONFIG_LOCATION="${arg#*=}"
		shift
		;;
		--file-name=*)
		FILE_NAME="${arg#*=}"
		shift
		;;
		--help)
		TYPE="SOME INVALID TYPE"
		CLEANUP_SCRIPT_CONFIG_LOCATION="some invalid migration script location"
		SHOW_HELP="yes"
		;;
		*)
		usage_
		exit 0
    esac
done

echo "$info_tag GNF CLEANUP CONFIGURATION IMPORT SCRIPT"
echo "$info_tag GLOBALPAYMENTS INC."
echo 
preliminary_validations_

if [ "$TYPE" = "GCS" ]
then
	echo "$info_tag The configuration import medium has been selected as Google Cloud Storage"
	echo "$info_tag Attempting to import configuration file from $GCS_BUCKET_NAME bucket"
	import_config_from_gcs_bucket_
elif [ "$TYPE" = "REMOTE" ]
then
	echo "$info_tag The configuration import medium has been selected as Remote Unix Machine"
	echo "$info_tag Attempting to import the configuration file from "
	echo "$info_tag REMOTE UNIX HOST: $SOURCE_HOST"
	echo "$info_tag USER NAME: $SOURCE_USER_NAME"
	echo "$info_tag REMOTE FILE PATH: $SOURCE_PATH" 
	import_config_from_remote_location_
elif [ "$TYPE" = "LOCAL" ]
then
	echo "$info_tag The Configuration import medium has been selected as Local Unix Machine"
	echo "$info_tag Attempting to import the configuration file from LOCAL SOURCE PATH: "
	echo "$info_tag $LOCAL_FILESYSTEM_SOURCE_PATH"
	echo "$info_tag "
	import_config_file_from_local_filesystem_
else
	echo "$error_tag You might have entered an invalid type; check out the help document for possible values"
	usage_
	exit 1
fi