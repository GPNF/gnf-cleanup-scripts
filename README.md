Cleanup script for Global Notification Framework

Commands


Commands
######gnf migration script

sh ~/gnf-migration-scripts/gnf-migration.sh \
--type=LOCAL \
--local-source-filepath=/home/bhagyashree_mehare/playground/config-store-local \
--file-name=config \
--log-dir=/home/$(whoami)/new_logs/gnf

######gnf cleanup
===================

###pubsub cleanup
-----------------
bash ~/gnf-cleanup-scripts/pubsub-cleanup/pubsub-cleanup-script.bash \
--source-project-id=pid-gousenapb-noti-res02 \
--specific-topic-to-be-deleted=to_be_deleted_topic \
--specific-subscription-to-be-deleted=to_be_deleted_subscription

###Cloudsql cleanup
-------------------
bash ~/gnf-cleanup-scripts/cloudsql-cleanup/cloud-sql-cleanup-script.bash \
--source-project-id=pid-gousenapb-noti-res02 \
--instance-name=sample-inst-six

###Appengine Cleanup
bash ~/gnf-cleanup-scripts/appengine-cleanup/gnf-appengine-cleanup.bash \
--source-project-id=pid-gousenapb-noti-res02 \
--service-name=some-example-service-delete 




##config import
LOCAL
bash ~/gnf-cleanup-scripts/config-import/config-import-script.bash \
--type=LOCAL \
--local-source-filepath=/home/bhagyashree_mehare/playground/config-store-local \
--file-name=cleanup_config \
--cleanup-script-config-location=/home/bhagyashree_mehare/gnf-cleanup-scripts/config-store/config_cleanup.txt

gcs
bash ~/gnf-cleanup-scripts/config-import/config-import-script.bash \
--type=GCS \
--gcs-bucket=gnf-cleanup-config-store \
--file-name=CONFIG \
--cleanup-script-config-location=/home/bhagyashree_mehare/gnf-cleanup-scripts/config-store/config_cleanup.txt




CLEANUP SCRIPT MAIN
sh ~/gnf-cleanup-scripts/gnf-cleanup-script.sh \
--type=LOCAL \
--local-source-filepath=/home/bhagyashree_mehare/playground/config-store-local \
--file-name=cleanup_config