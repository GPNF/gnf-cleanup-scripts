#/bin/bash
#this script will simply delete the cloud sql instance(s)
SOURCE_PROJECT_ID=""
DELETE_ALL_INSTANCES="no"
NAME_OF_THE_SPECIFIC_INSTANCE_TO_BE_DELETED=""
SHOW_HELP="no"
#array
declare -a INSTANCE_ARR=()

#system vars
info_tag="[INFO: CLOUD_SQL_CLEANUP]"
error_tag="[ERROR: CLOUD_SQL_CLEANUP]"
#sysvars
temp_file_dir="/home/$(whoami)/cleanup_tmp"
temporary_cloudsql_instance_file_name="tempInstanceFile"
temp_cloudsql_instance_details_file_path="${temp_file_dir}/${temporary_cloudsql_instance_file_name}"

usage_() {
	echo "Usage will appear here"
}

set_project_() {
	gcloud config set project ${SOURCE_PROJECT_ID}
	if [ "$?" -eq 0 ] 
	then
		echo "$info_tag Project successfully set as $SOURCE_PROJECT_ID"
		echo "$info_tag Removing temporary instance file"
	    rm -f ${temp_cloudsql_instance_details_file_path}
	    
	    chmod 644 ${temp_cloudsql_instance_details_file_path}
	else
		echo "$error_tag Could not set project as $SOURCE_PROJECT_ID"
		echo "$error_tag Please check the gcloud logs above"
		echo "$error_tag or set a valid project id"
	fi
}	


list_cloudsql_instances_() {
	gcloud sql instances list >> ${temp_cloudsql_instance_details_file_path}
	instanceList=$(cat ${temp_cloudsql_instance_details_file_path} | grep -v "NAME" | cut -d" " -f1)
	INSTANCE_ARR=($instanceList)

	echo "$info_tag Cloud SQL Instances present in $SOURCE_PROJECT_ID are given below "
	for instance_name in "${INSTANCE_ARR[@]}"; do
		#statements
		echo "$info_tag ${instance_name}"
	done
}

delete_instance_() {
	instance_name=${1}
	echo "$info_tag Attempting to delete ${instance_name}"
	(sleep 5; echo y; sleep 2; echo n) | gcloud sql instances delete ${instance_name}
	gcloud_exit_code=${?}
	if [ "${gcloud_exit_code}" -eq 0 ]
	then
		echo "$info_tag Successfully deleted ${instance_name}"
	else
		echo "$error_tag Could not delete ${instance_name}"
		echo "$error_tag Check for the gcloud execution logs above"
		echo "$error_tag . (EXIT CODE ${gcloud_exit_code})"
		exit 100
	fi
}

delete_all_instances_() {
	echo "$info_tag Attempting to delete all instances"
	for instance_name in "${INSTANCE_ARR[@]}"; do
		#statements
		delete_instance_ ${instance_name}
	done
}

preliminary_validations_(){
	if [ "$SHOW_HELP" = "yes" ]
	then
		usage_
		exit 0
	fi

	echo "$info_tag Validating the inputs provided by the user"
	if [ -z "$SOURCE_PROJECT_ID" ]
	then
		echo "$error_tag Source Project Id cannot be empty"
		usage_
		exit 1
	elif [ "$DELETE_ALL_INSTANCES" = "no" ]
	then
		echo "$info_tag You have not selected the --all flag"
		echo "$info_tag Verifying whether you have provided "
		echo "$info_tag the specific instance name which you intend to delete"

		if [ -z "$NAME_OF_THE_SPECIFIC_INSTANCE_TO_BE_DELETED" ]
		then
			echo "$error_tag When --all is not selected; the user has to provide"
			echo "$error_tag the name of the instance which he/she intends to delete"
			usage_
		else 
			echo "$info_tag Name of the instance to be deleted has been"
			echo "$info_tag provided by the user is $NAME_OF_THE_SPECIFIC_INSTANCE_TO_BE_DELETED"
		fi
	else
		echo "$info_tag All validations complete; Continuing with"
		echo "$info_tag Cloud SQL cleanup"
	fi
}

for arg in "$@"
do
    case $arg in
        #PROJECT-ID
       	 -spid=*|--source-project-id=*)
		 SOURCE_PROJECT_ID="${arg#*=}"
		 shift
		 ;;
		 --all)
		 DELETE_ALL_INSTANCES="yes"
		 shift
		 ;;
		 --instance-name=*)
		 NAME_OF_THE_SPECIFIC_INSTANCE_TO_BE_DELETED="${arg#*=}"
		 shift
		 ;;
		 -h|--help)
		 SHOW_HELP="yes"
		 shift
		 ;;
		 *)
		SHOW_HELP="yes"
		exit 1
		 ;;
    esac
done

echo "$info_tag CLOUD SQL CLEANUP"
echo "$info_tag GLOBALPAYMENTS INC."
echo 
preliminary_validations_
echo "$info_tag Setting project as $SOURCE_PROJECT_ID"
set_project_
echo "$info_tag Fetching CloudSQL instance details from the project"
echo "$info_tag and listing them"
echo 
list_cloudsql_instances_
if [ "$DELETE_ALL_INSTANCES" = "no" ]
then
	echo "$info_tag --all tag has not been selected; proceeding with deleting the instance"
	echo "$info_tag name as provided by the user"
	delete_instance_ ${NAME_OF_THE_SPECIFIC_INSTANCE_TO_BE_DELETED}
else
	delete_all_instances_
fi
echo "$info_tag End of Process . ."